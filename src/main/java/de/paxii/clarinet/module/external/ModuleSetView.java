package de.paxii.clarinet.module.external;

import de.paxii.clarinet.Wrapper;
import de.paxii.clarinet.event.EventHandler;
import de.paxii.clarinet.event.events.game.IngameTickEvent;
import de.paxii.clarinet.module.Module;
import de.paxii.clarinet.module.ModuleCategory;
import de.paxii.clarinet.util.chat.Chat;
import de.paxii.clarinet.util.player.PlayerUtils;

import net.minecraft.entity.player.EntityPlayer;

/**
 * Created by Lars on 27.08.2016.
 */
public class ModuleSetView extends Module {
  private EntityPlayer target;

  public ModuleSetView() {
    super("SetView", ModuleCategory.RENDER);

    this.setBuildVersion(17500);
    this.setVersion("1.1");
    this.setDisplayedInGui(false);
    this.setCommand(true);
    this.setSyntax("setview <player>");
    this.setDescription("Changes the view entity to the given player.");
  }

  @Override
  public void onEnable() {
    Wrapper.getEventManager().register(this);
  }

  @EventHandler
  public void onIngameTick(IngameTickEvent tickEvent) {
    if (this.target != null && Wrapper.getPlayer().getDistanceToEntity(this.target) >= 45) {
      Wrapper.getMinecraft().setRenderViewEntity(Wrapper.getPlayer());
      Chat.printClientMessage(String.format("%s is too far away now.", this.target.getName()));
      this.target = null;
    }
  }

  @Override
  public void onCommand(String[] args) {
    if (args.length > 0) {
      String player = args[0];

      if (player.equals("self") || player.equals(Wrapper.getPlayer().getName())) {
        Wrapper.getMinecraft().setRenderViewEntity(Wrapper.getPlayer());
        Chat.printClientMessage("You are now yourself again.");
        this.setEnabled(false);
        return;
      }

      EntityPlayer entityPlayer = PlayerUtils.getPlayerByName(args[0]);
      if (entityPlayer != null) {
        this.target = entityPlayer;
        Wrapper.getMinecraft().setRenderViewEntity(entityPlayer);
        Chat.printClientMessage(String.format("You are now seeing the World as %s.", entityPlayer.getName()));
        this.setEnabled(true);
      } else {
        Chat.printClientMessage(String.format("The Player %s could not be found!", player));
      }
    }
  }

  @Override
  public void onDisable() {
    Wrapper.getEventManager().unregister(this);
  }
}
